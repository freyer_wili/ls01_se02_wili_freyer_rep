import java.util.ArrayList;
import java.util.Scanner;

public class Benutzerverwaltung {

	public static void main(String[] args) {

		ArrayList<Benutzer> benutzerliste = new ArrayList<Benutzer>();
		int auswahl;

		do {
			auswahl = menu();
			switch (auswahl) {
			case 1:
				benutzerAnzeigen(benutzerliste);
				
				break;
			case 2:
				benutzerErfassen(benutzerliste);
				break;
			case 3:
				// benutzerLöschen();
				break;
			case 4:
				System.exit(0);
			default:
				System.err.println("\nFalsche Eingabe.\n");
			}
		} while (true);

	}

	public static int menu() {

		int selection;
		Scanner input = new Scanner(System.in);

		/***************************************************/

		System.out.println("     Benutzerverwaltung     ");
		System.out.println("----------------------------\n");
		System.out.println("1 - Benutzer anzeigen");
		System.out.println("2 - Benutzer erfassen");
		System.out.println("3 - Benutzer löschen");
		System.out.println("4 - Ende");

		System.out.print("\nEingabe:  ");
		selection = input.nextInt();
		return selection;
	}

	public static void benutzerAnzeigen(ArrayList<Benutzer> benutzerliste) {
		System.out.println(benutzerliste);
	}
	
	public static void benutzerErfassen(ArrayList<Benutzer> benutzerliste) {
		
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Geben sie die Benutzernummer ein: ");
		int nummer = tastatur.nextInt();
		System.out.println("Geben sie den Vornamen ein: ");
		String vname = tastatur.next();
		System.out.println("Geben sie die Nachnamen ein: ");
		String nname = tastatur.next();
		System.out.println("Geben sie die Geburtsjahr ein: ");
		int gjahr = tastatur.nextInt();
		  //Nummer , Vorname, Nachname und Geburtsjahr von der Tastatur lesen.
			
		Benutzer b1 = new Benutzer(nummer, vname, nname, gjahr);			
		benutzerliste.add(b1);
		
			
		}

}