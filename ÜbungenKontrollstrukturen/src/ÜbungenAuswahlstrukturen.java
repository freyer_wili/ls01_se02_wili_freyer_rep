
import java.util.Scanner;
public class ÜbungenAuswahlstrukturen {

	public static void main(String[] args) {
		
		int zahl1 = 0;
		int zahl2 = 0;
		int zahl3 = 0;
		
		zahl1 = EingabeErfassen();
		zahl2 = EingabeErfassen2();
		zahl3 = EingabeErfassen3();
		String erg = eingegebeneZahl(zahl1, zahl2, zahl3);
		System.out.printf(erg);                                

	}
	
	public static int EingabeErfassen () {
		Scanner myScanner=new Scanner(System.in);
		System.out.print("Wie lautet die erste Zahl?: ");
		int zahl = myScanner.nextInt();
		return zahl;
	}
	
	public static int EingabeErfassen2 () {
		Scanner myScanner=new Scanner(System.in);
		System.out.print("Wie lautet die zweite Zahl?: ");
		int zahl2 = myScanner.nextInt();
		return zahl2;
	}
	
	public static int EingabeErfassen3 () {
		Scanner myScanner=new Scanner(System.in);
		System.out.print("Wie lautet die dritte Zahl?: ");
		int zahl3 = myScanner.nextInt();
		return zahl3;
	}
	
	public static String eingegebeneZahl (int zahl1, int zahl2, int zahl3) {
		
		String erg = "";
		
		if (zahl1 > zahl2 && zahl1 > zahl3) {
			erg="Die erste Zahl ist größer als die zweite und dritte Zahl";
		}
		else if (zahl2 > zahl1 && zahl2 > zahl3) {
			erg="Die zweite Zahl ist größer als die erste und dritte Zahl";
		}
		else if (zahl3 > zahl1 && zahl3 > zahl2) {
			erg="Die dritte Zahl ist größer als die erste und zweite Zahl";
		}
		
		return erg;
		
		}
	

}
