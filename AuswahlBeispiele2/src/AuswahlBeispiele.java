

public class AuswahlBeispiele {

	public static void main(String[] args) {

		int z1 = 5;
		int z2 = 7;
		double erg = max2(2.5, 7.2, 3.6); // 7.2

		System.out.printf("Ergebnis:" + erg);
	}

	public static int min(int zahl1, int zahl2) {

		int erg;
		if (zahl1 < zahl2) {
			erg = zahl1;
		} else {
			erg = zahl2;

		}
		return erg;
	}

	// 2. Methode
	public static int max(int zahl1, int zahl2) {

		int erg;
		if (zahl1 < zahl2) {
			erg = zahl1;
		} else {
			erg = zahl2;
		}
		return erg;
	}

	// 3.Methode
	public static double max2(double zahl1,double zahl2,double zahl3) {
				
				double erg = 0;
				if (zahl1 > zahl2) {
					erg=zahl1;
				}
				else if (zahl1 > zahl3) {
					erg=zahl1;
				}
				else if (zahl2 > zahl1) {
					erg=zahl2;
				}
				else if (zahl2 > zahl3) {
					erg=zahl2;
				}
				else if (zahl3 > zahl1) {
					erg=zahl3;
				}
				else if (zahl3 > zahl2) {
					erg=zahl3;
				}
				return erg;
			}

}

