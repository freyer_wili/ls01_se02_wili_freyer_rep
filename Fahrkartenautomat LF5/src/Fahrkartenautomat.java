﻿import java.util.Scanner;

class Fahrkartenautomat{
    public static void main(String[] args) {
    	
    	
       double zuZahlenderBetragTicket;
       double eingezahlterGesamtbetrag = 0.0;
       double eingeworfeneMünze;
       //double rückgabebetrag;
       double zuZahlenderBetrag; 
       
       zuZahlenderBetrag = fahrkartenbestellungErfassen();
       eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
       
       rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
    }
    
    

   // 1.Methode Bestellen
    public static double fahrkartenbestellungErfassen() {
    	
    	 Scanner tastatur = new Scanner(System.in); 
    	 System.out.print("Ticketpreis (EURO): \n");
         double zuZahlenderBetragTicket = tastatur.nextDouble();
         System.out.print("Anzahl der Tickets: \n");
         int anzahlticket = tastatur.nextInt();
         double zuZahlenderBetrag = (anzahlticket*zuZahlenderBetragTicket); 
         zuZahlenderBetrag *=100;
         return zuZahlenderBetrag;
    }
    
    //2.Methode Geldeinwurf
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {   
    	
    	double eingezahlterGesamtbetrag = 0.0;
		double rückgabebetrag = 0;
		
    	while(eingezahlterGesamtbetrag < zuZahlenderBetrag) {
    		Scanner tastatur = new Scanner(System.in); 
    		System.out.printf("Noch zu zahlen: %.2f Euro%n",(zuZahlenderBetrag - eingezahlterGesamtbetrag)/100);
    		System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    		double eingeworfeneMünze = tastatur.nextDouble()*100;
    		eingezahlterGesamtbetrag += eingeworfeneMünze;	
    	} 
    	return eingezahlterGesamtbetrag;
    }
    
    //3. Methode Fahrkartenausgabe
    public static void fahrkartenausgabe() {
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 			}
        }
        System.out.println("\n\n");
    	
    }
    
    //4. Methode Rückgeldausgabe
 // Rückgeldberechnung und -Ausgabe
    // -------------------------------
    public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
    double rückgabebetrag =(eingezahlterGesamtbetrag - zuZahlenderBetrag);
    if(rückgabebetrag > 0.0)
    {
 	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO%n",(rückgabebetrag)/100);
 	   System.out.println("wird in folgenden Münzen ausgezahlt:");

        while(rückgabebetrag >= 200) // 2 EURO-Münzen
        {
     	  System.out.println("2 EURO");
	          rückgabebetrag -= 200;
        }
        while(rückgabebetrag >= 100) // 1 EURO-Münzen
        {
     	  System.out.println("1 EURO");
	          rückgabebetrag -= 100;
        }
        while(rückgabebetrag >= 50) // 50 CENT-Münzen
        {
     	  System.out.println("50 CENT");
	          rückgabebetrag -= 50;
        }
        while(rückgabebetrag >= 20) // 20 CENT-Münzen
        {
     	  System.out.println("20 CENT");
	          rückgabebetrag -= 20;
        }
        while(rückgabebetrag >= 10) // 10 CENT-Münzen
        {
     	  System.out.println("10 CENT");
	          rückgabebetrag -= 10;
        }
        while(rückgabebetrag >= 5)// 5 CENT-Münzen
        {
     	  System.out.println("5 CENT");
	          rückgabebetrag -= 5;
        }
    }

    System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                       "vor Fahrtantritt entwerten zu lassen!\n"+
                       "Wir wünschen Ihnen eine gute Fahrt.");
    	}
	}

    
    	
