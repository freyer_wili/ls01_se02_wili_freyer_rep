﻿import java.util.Scanner;

class Fahrkartenautomat{
    public static void main(String[] args){
    	
    	
       double zuZahlenderBetragTicket;
       double eingezahlterGesamtbetrag;
       double eingeworfeneMünze;
       double rückgabebetrag;
       double zuZahlenderBetrag; 

       
  //   System.out.print("Wählen Sie Ihre Wunschfahrkarte für Berlin aus:\n");
  //   System.out.print("   Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\n");
  //   System.out.print("   Tageskarte Regeltarif AB [8,60 EUR] (2)\n");
  //   System.out.print("   Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\n");  

  	   
       zuZahlenderBetrag = fahrkartenbestellungErfassenTeil1();
       eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
       fahrkartenAusgeben();
       rueckgeldAusgeben(eingezahlterGesamtbetrag,zuZahlenderBetrag);
       wiederholung();
                                
    }
   // 1.1Methode ticketpreis
    public static double fahrkartenbestellungErfassenTeil1() {
    	double preis = 0;
    	double betrag = 0;
    	int anzahl = 0;
    	
    	String [] ticketname = {"Einzelfahrschein Berlin AB","Einzelfahrschein Berlin BC","Einzelfahrschein Berlin ABC","Kurzstrecke","Tageskarte Berlin AB","Tageskarte Berlin BC","Tageskarte Berlin ABC","Kleingruppen-Tageskarte Berlin AB","Kleingruppen-Tageskarte Berlin BC","Kleingruppen-Tageskarte Berlin ABC"};
    	double [] ticketpreis = {2.90,3.30,3.60,1.90,8.60,9.00,9.60,23.50,24.30,24.90};
    	
    	 System.out.println("Bitte wählen Sie eine Fahrkarte:");
    	
    	for (int i = 0; i < ticketpreis.length; i++ ) {
    	System.out.printf("(" + (i+1) +") " + ticketname[i] + " [%.2f EUR]" + "%n", ticketpreis[i]);
    	}
    	 
     	 Scanner tastatur = new Scanner(System.in);
     	 int karte  = tastatur.nextInt();
     	 
     	 if (karte < 1 || karte > 10) {
     		System.out.println("Error");
     		fahrkartenbestellungErfassenTeil1();
     	 }
     	 
     	 else 
     		 System.out.printf("Ihre Wahl: (" + karte + ")" + ticketname[karte-1] + " [%.2f EUR] pro Ticket", ticketpreis[karte-1]);
     	 	preis = ticketpreis[karte-1];
     	 	
        	System.out.print("\nAnzahl der Tickets eingeben (MAX. 10): \n");
        	anzahl = tastatur.nextInt();            
        	while (anzahl < 1 || anzahl > 10) {	 
        		System.out.print("\nAnzahl der Tickets erneut eingeben (MAX. 10): \n");
       		 	anzahl = tastatur.nextInt();  		 	
        	}
    
        	betrag = anzahl* (preis*100);
        	
        	return betrag; }
        	

     
    //2.Methode Bezahlung
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {   
    	
    	double eingezahlterGesamtbetrag = 0.0;
		double rückgabebetrag = 0.0;
		
    	while(eingezahlterGesamtbetrag < zuZahlenderBetrag) {
    		Scanner tastatur = new Scanner(System.in); 
    		System.out.printf("\nNoch zu zahlen: %.2f Euro%n",(zuZahlenderBetrag - eingezahlterGesamtbetrag) / 100);
    		System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    		double eingeworfeneMünze = tastatur.nextDouble() * 100;
    		eingezahlterGesamtbetrag += eingeworfeneMünze;	
    	} 
    	return eingezahlterGesamtbetrag;
    }
    
    //3.Methode Fahrkartenausgabe
    public static void fahrkartenAusgeben() {
    
    	System.out.println("\nFahrschein wird ausgegeben");
    	for (int i = 0; i < 26; i++)
    	{
    		System.out.print("=");
    		try {
    			Thread.sleep(10);
    		} catch (InterruptedException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    	}
    	System.out.println("\n\n");  
		
    }
    
    //5.Methode Rückgeld ausgeben
    public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
    	
    	double rückgabebetrag = 0;
    	
        rückgabebetrag = (eingezahlterGesamtbetrag - zuZahlenderBetrag) / 100;
        if(rückgabebetrag > 0.0)
        {
     	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO%n",(rückgabebetrag));
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.50) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rückgabebetrag -= 0.50;
            }
            while(rückgabebetrag >= 0.20) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 0.20;
            }
            while(rückgabebetrag >= 0.10) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 0.10;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rückgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.");
    }
    
    public static void wiederholung() {
    	Scanner tastatur = new Scanner(System.in);
    	System.out.print("\nWollen Sie noch mehr Fahrkarten kaufen? [Ja/Nein]\n\n");
    	String antwort = tastatur.next();
    	if (antwort.equals("JA") || antwort.equals("ja") || antwort.equals("Ja") || antwort.equals("j")) {
    		fahrkartenbestellungErfassenTeil1();
    	}
    	else 
    		System.out.print("\nBesuchen Sie uns bald wieder!\n");
    		System.exit(1);
    	
    }
}



    	
