import java.util.Scanner;
public class BeispielSchleife {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Geben sie ihre Zahl ein:");
		int eingabeZahl = myScanner.nextInt();
		
		int zaehler=1;
		int sum = 0;
		
		while (zaehler <= eingabeZahl) {
			
			System.out.print(zaehler);
			
			if (zaehler == eingabeZahl) {
				System.out.print(" = ");
				
			}
			else {
			System.out.print(" + ");
			
			sum = sum + zaehler;
		
			zaehler++;
			}	
	
		System.out.println(sum);
	}
	}	
}
