public class Ladungtest {

	public static void main(String[] args) {
		
		Ladung l1 = new Ladung();
	
		l1.setBezeichnung("Ferengi Schneckensaft");
		l1.setMenge(200);
		System.out.println("Bezeichnung: "+l1.getBezeichnung());
		System.out.println("Menge: "+l1.getMenge());
		System.out.println("----------------------------------------");
		
		Ladung l2 = new Ladung();
		
		l2.setBezeichnung("Berg-Schrott");
		l2.setMenge(5);
		System.out.println("Bezeichnung: "+l2.getBezeichnung());
		System.out.println("Menge: "+l2.getMenge());
		System.out.println("----------------------------------------");
		
		Ladung l3 = new Ladung();
		
		l3.setBezeichnung("Rote Materie");
		l3.setMenge(2);
		System.out.println("Bezeichnung: "+l3.getBezeichnung());
		System.out.println("Menge: "+l3.getMenge());
		System.out.println("----------------------------------------");
		
		Ladung l4 = new Ladung();
		
		l4.setBezeichnung("Forschungssonde");
		l4.setMenge(35);
		System.out.println("Bezeichnung: "+l4.getBezeichnung());
		System.out.println("Menge: "+l4.getMenge());
		System.out.println("----------------------------------------");
		
		Ladung l5 = new Ladung();
		
		l5.setBezeichnung("Photonentorpedo");
		l5.setMenge(3);
		System.out.println("Bezeichnung: "+l5.getBezeichnung());
		System.out.println("Menge: "+l5.getMenge());
		System.out.println("----------------------------------------");
		
		Ladung l6 = new Ladung();
		
		l6.setBezeichnung("Plasma-Waffe");
		l6.setMenge(50);
		System.out.println("Bezeichnung: "+l6.getBezeichnung());
		System.out.println("Menge: "+l6.getMenge());
		System.out.println("----------------------------------------");
		
		Ladung l7 = new Ladung();
		
		l7.setBezeichnung("Bat�leth Klingonen Schwert");
		l7.setMenge(200);
		System.out.println("Bezeichnung: "+l7.getBezeichnung());
		System.out.println("Menge: "+l7.getMenge());
		System.out.println("----------------------------------------");
		

	}

}